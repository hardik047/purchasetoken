# Purchase Token
A new quantity will be added to the total token supply on every new purchase.

#### Quick demo
```
cd client

chmod 777 start.sh

./start.sh
```

#### Using the client
```
//To enroll a new identity on the blockchain and getting certs in a new wallet
node enroll.js

//To submit a transaction
node client.js