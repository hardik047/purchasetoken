const {FileSystemWallet, Gateway} = require('fabric-network');
const path = require('path');

const ccpPath = path.resolve('connection.json');

async function main() {
    try {

        const walletPath = path.join(process.cwd(), 'wallet');
        const wallet = new FileSystemWallet(walletPath);

        const gateway = new Gateway();
        await gateway.connect(ccpPath, {wallet, identity: 'admin', discovery: {enabled: true, asLocalhost: true}});

        const network = await gateway.getNetwork('mychannel');
        const contract = await network.getContract('token-contract');

        var responseInBytes = await contract.submitTransaction('generateNewToken', 'pur12345');
        // contract.addContractListener('purchaseVerifier', function (error, payload, blocknumber, txid, status) {
        //     console.log(txid);
        // });
        var response = JSON.parse(responseInBytes.toString());
        console.log('Transaction has been submitted. response: ', response.txid);

        await gateway.disconnect();

    } catch(e) {
        console.error('Failed to submit the transaction: ', e);
        process.exit(1);
    }
}

main();