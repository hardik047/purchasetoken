const fabricCAServices = require('fabric-ca-client');
const {FileSystemWallet, X509WalletMixin} = require('fabric-network');
const fs = require('fs');
const path = require('path');

const ccpPath = path.resolve('connection.json');
const ccpJSON = fs.readFileSync(ccpPath, 'utf-8');
const ccp = JSON.parse(ccpJSON);

async function main() {

    try {
        console.log('started');
        const caInfo = ccp.certificateAuthorities['ca.example.com'];
        const caTLSCACerts = fs.readFileSync(caInfo.tlsCACerts.path);
        const ca = new fabricCAServices(caInfo.url, {trustedRoots: caTLSCACerts, verify: false}, caInfo.caName);

        const walletPath = path.join(process.cwd(), 'wallet');
        console.log(`Wallet path: ${walletPath}`);
        const wallet = new FileSystemWallet(walletPath);

        const adminExists = await wallet.exists('admin');
        if(adminExists) {
            console.log('An identity for the admin user "admin" already exists in the wallet');
            return;
        }

        const enrollment = await ca.enroll({
            enrollmentID: 'admin',
            enrollmentSecret: 'adminpw'
        });
        const identity = X509WalletMixin.createIdentity('Org1MSP', enrollment.certificate, enrollment.key.toBytes());
        await wallet.import('admin', identity);
        console.log('Successfully enrolled admin user "admin" and imported it into the wallet');

    } catch(e) {
        console.error(`Failed to enroll admin user "admin": ${e}`);
        process.exit(1);
    }

}

main();