'use strict';
const { Contract} = require('fabric-contract-api');

class tokenContract extends Contract {

  async generateNewToken(ctx, purchaseTxID) {

    let currentBalanceAsBytes = await ctx.stub.getState('tokenbalance');
    if(!currentBalanceAsBytes || currentBalanceAsBytes.toString().length <= 0) {
      await ctx.stub.putState('tokenbalance', Buffer.from('1'));
      return {
        message: 'newledger',
        txid: ctx.stub.getTxID()
      };
    }

    let newBalance = parseInt(currentBalanceAsBytes.toString()) + 1;
    await ctx.stub.putState('tokenbalance', Buffer.from(newBalance.toString()));
    return {
      message: 'success',
      txid: ctx.stub.getTxID()
    };

  }

}

module.exports=tokenContract;
